package com.example.android_project.ActivitiesPlantes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.android_project.ActivitiesFamille.FamilleListeActivity;
import com.example.android_project.ActivitiesRecette.RecetteListActivity;
import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.PlanteDAO;

import com.example.android_project.MainActivity;
import com.example.android_project.R;
import com.example.android_project.RecetteListePlanteActivity;
import com.example.android_project.Tools.ItemClickListener;
import com.example.android_project.Tools.Plante.PlanteAdapter;
import com.example.android_project.Tools.Plante.PlanteViewModel;



public class PlantesListeActivity extends AppCompatActivity implements ItemClickListener {

    RecyclerView recyclerView;
    PlanteDAO planteDAO;
    TextView nbTotalPlantesLD;
    PlanteViewModel planteViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plante_activity_liste);

        planteViewModel = new ViewModelProvider(this).get(PlanteViewModel.class);

        //nombre total de plantes
        nbTotalPlantesLD = findViewById(R.id.textViewTotalPlantes);
        planteViewModel.getNbPlantesLD().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                nbTotalPlantesLD.setText("Total Plantes : " + integer);
            }
        });

        recyclerView = findViewById(R.id.PlanteRecyclerView);
        planteDAO = AppDatabase.getDatabaseInstance(this).getPlanteDAO();
        PlanteAdapter planteAdapter = new PlanteAdapter(planteDAO.getPlantes(),this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(planteAdapter);

        Log.d("meslogs", "planteDAO  " +planteDAO.getPlantes());




    }


//-------------------------------------------------------------------------------------------

    @Override
    public void onShowClick(int position) {
        Log.d("meslogs", "test bouton cliqué show  " +position);
        Intent intent = new Intent(this, PlanteDetailActivity.class);
        intent.putExtra("plante_selected", planteDAO.getPlantes().get(position));
        startActivity(intent);
    }
    @Override
    public void onUpdateClick(int position) {
        //Log.d("meslogs", "test bouton cliqué update  "+ position);
        Intent intent = new Intent(this, PlanteUpdateActivity.class);
        intent.putExtra("updatePlante", planteDAO.getPlantes().get(position));
        startActivity(intent);
    }

    @Override
    public void onDeleteClick(int position) {
        //Log.d("meslogs", "test bouton cliqué delete  " + position );
        planteDAO = AppDatabase.getDatabaseInstance(this).getPlanteDAO();
        PlanteAdapter planteAdapter = new PlanteAdapter(planteDAO.getPlantes(),this);
        planteViewModel.delete(planteAdapter.getLaPlante(position));
        recreate();
    }


    //------------------------ BARRE MENU ------------------------------------------------------

    public void LienHome(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void listeRecettes(View view){
        Intent intent = new Intent(this, RecetteListActivity.class);
        startActivity(intent);
    }

    public void listeFamilles(View view){
        Intent intent = new Intent(this, FamilleListeActivity.class);
        startActivity(intent);
    }

    public void listePlantes(View view){
        Intent intent = new Intent(this, PlantesListeActivity.class);
        startActivity(intent);
    }

    //pour ajouter une plante via la barre de menu
    public void AjouterPlus(View view) {
        Intent intent = new Intent(this, PlanteMainActivity.class);
        startActivity(intent);

    }


}

