package com.example.android_project.ActivitiesPlantes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android_project.Model.Plante;
import com.example.android_project.R;
import com.example.android_project.Tools.Plante.PlanteViewModel;

public class PlanteUpdateActivity extends AppCompatActivity {

    EditText nom, nomS, description;
    PlanteViewModel planteViewModel;
    //PlanteDAO planteDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plante_activity_update);

        planteViewModel = new ViewModelProvider(this).get(PlanteViewModel.class);

        nom = findViewById(R.id.editTextPlanteNomEdit);
        nomS = findViewById(R.id.editTextPlanteNomSEdit);
        description = findViewById(R.id.editTextPlanteDescriptionEdit);

        if(getIntent().hasExtra("updatePlante")){
            Plante plante = getIntent().getParcelableExtra("updatePlante");
            nom.setText(plante.getNomCommun());
            nomS.setText(plante.getNomScientifique());
            description.setText(plante.getDescription());
            Log.d("meslogs ","test données plante : " + plante.toString());
        }


       // planteDAO = AppDatabase.getDatabaseInstance(this).getPlanteDAO();

    }

    public void updatePlante(View view) {

        Plante plante = getIntent().getParcelableExtra("updatePlante");
        plante.setNomCommun(nom.getText().toString());
        plante.setNomScientifique(nomS.getText().toString());
        plante.setDescription(description.getText().toString());

        planteViewModel.update(plante);
        Intent intent = new Intent(this, PlantesListeActivity.class);

        Log.d("meslogs", "update Plante :  " + plante.toString());

        Toast.makeText(this, "Plante modifié !", Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }



    public void cancelUpdate(View view) {
        finish();
    }
}