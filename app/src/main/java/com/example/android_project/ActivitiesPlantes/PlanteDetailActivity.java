package com.example.android_project.ActivitiesPlantes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.TextView;

import com.example.android_project.Model.Plante;
import com.example.android_project.R;

public class PlanteDetailActivity extends AppCompatActivity {

    TextView famille,nom, nomS, description;

    Plante plante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plante_activity_detail);

        famille = findViewById(R.id.tvFamilleNomDetail);
        nom = findViewById(R.id.tvPlanteNomDetail);
        nomS = findViewById(R.id.tvPlanteNomScientifiqueDetail);
        description = findViewById(R.id.tvPlanteDescriptionDetail);

        if(getIntent().hasExtra("plante_selected")){
            Plante plante = getIntent().getParcelableExtra("plante_selected");
            famille.setText(String.valueOf(plante.getFamilleId()));
            nom.setText(plante.getNomCommun());
            nomS.setText(plante.getNomScientifique());
            description.setText(plante.getDescription());

            Log.d("meslogs", "plante.getIdFamille " +  plante.toString());
        }
    }

    public void retourListePlante(View v){
        finish();
    }

}
