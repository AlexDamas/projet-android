package com.example.android_project.ActivitiesPlantes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android_project.BD.PlanteDAO;
import com.example.android_project.BD.AppDatabase;
import com.example.android_project.Model.Famille;
import com.example.android_project.Model.FamillePlantes;
import com.example.android_project.Model.Plante;
import com.example.android_project.R;

import java.util.ArrayList;
import java.util.List;

public class PlanteMainActivity extends AppCompatActivity {

    private EditText nom, nomScientifique, description;

    List<Famille> famPlante;

    Integer famIdPositon;

    private Spinner spinner;

    PlanteDAO planteDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plante_activity_main);

        nom = findViewById(R.id.editTextPlanteNom);
        nomScientifique = findViewById(R.id.editTextPlanteNomScientifique);
        description = findViewById(R.id.editTextPlanteDescription);
        spinner = findViewById(R.id.spinnerFamillle);

        // il faudrait normalement passer par le ViewModel
        famPlante = AppDatabase.getDatabaseInstance(this).getFamilleDAO().getFamilles();

        ArrayAdapter<Famille> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, famPlante);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                famIdPositon = 1 + pos;
                Log.d("planteslogs","position :" + pos + "famIdPositon :" + famIdPositon );
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        planteDAO = AppDatabase.getDatabaseInstance(this).getPlanteDAO();

    }


    //-------------------------------  SAUVEGARDER PLANTE  -------------------------------------

    //sauvegarder les informations de la plante

    public void SavePlante(View view) {
        if(nom.getText().toString().isEmpty() || nomScientifique.getText().toString().isEmpty() || description.getText().toString().isEmpty()) {
            Toast.makeText(this, "Il manque des infos", Toast.LENGTH_SHORT).show();
        }else{
            Plante plante = new Plante();
            plante.setNomCommun(nom.getText().toString());
            plante.setNomScientifique(nomScientifique.getText().toString());
            plante.setDescription(description.getText().toString());
            plante.setFamilleId(famIdPositon.intValue());

            planteDAO.insertPlante(plante);

            Log.d("planteslogs","plante.getIdFamille()" +  plante.getFamilleId());

            Toast.makeText(this, "Plante ajoutée !", Toast.LENGTH_SHORT).show();
        }
    }

    //---------------------------------- AFFICHER LES PLANTES ---------------------------------------

    public void ShowPlantes(View view) {
        Intent intent = new Intent(this, PlantesListeActivity.class);
        startActivity(intent);
    }



}
