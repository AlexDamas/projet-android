package com.example.android_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.android_project.ActivitiesPlantes.PlanteDetailActivity;
import com.example.android_project.ActivitiesPlantes.PlanteUpdateActivity;
import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.PlanteDAO;
import com.example.android_project.Model.FamillePlantes;
import com.example.android_project.Model.Plante;
import com.example.android_project.Tools.FamillePlante.FamillePlanteAdapter;
import com.example.android_project.Tools.ItemClickListener;
import com.example.android_project.Tools.Plante.PlanteAdapter;
import com.example.android_project.Tools.Plante.PlanteViewModel;

import java.util.ArrayList;
import java.util.List;

public class FamillePlanteListeActivity extends AppCompatActivity {

    PlanteViewModel planteViewModel;
    RecyclerView recyclerView;
    PlanteDAO planteDAO;
    Integer famId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plante_activity_show_2);

        recyclerView = findViewById(R.id.recetteRecyclerView);

        planteDAO = AppDatabase.getDatabaseInstance(this).getPlanteDAO();

        Intent in = getIntent();
        Bundle b = in.getExtras();
        famId = b.getInt("famille_plante");

        FamillePlanteAdapter famillePlanteAdapter = new FamillePlanteAdapter(planteDAO.getPlanteByFamilleId(famId));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(famillePlanteAdapter);

    }

}