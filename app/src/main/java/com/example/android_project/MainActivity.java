package com.example.android_project;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.android_project.ActivitiesFamille.FamilleListeActivity;
import com.example.android_project.ActivitiesFamille.FamilleMainActivity;
import com.example.android_project.ActivitiesPlantes.PlanteMainActivity;
import com.example.android_project.ActivitiesPlantes.PlantesListeActivity;
import com.example.android_project.ActivitiesRecette.RecetteListActivity;
import com.example.android_project.ActivitiesRecette.RecetteMainActivity;
import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.PlanteDAO;
import com.example.android_project.Model.Famille;
import com.example.android_project.Model.FamillePlantes;
import com.example.android_project.Model.Plante;
import com.example.android_project.Tools.Famille.FamilleViewModel;
import com.example.android_project.Tools.Plante.PlanteViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Spinner spinnerPlante;
    Spinner spinnerFamille;
    Button ajouterPlanteButton;
    Button ajouterRecetteButton;
    PlanteViewModel planteViewModel;
    FamilleViewModel familleViewModel;

    Integer planteIdPositon;
    Integer famIdPositon;

    List<Plante> listePlante = new ArrayList<>();
    List<Famille> listeFamille = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        familleViewModel = new ViewModelProvider(this).get(FamilleViewModel.class);
        planteViewModel = new ViewModelProvider(this).get(PlanteViewModel.class);

        ajouterPlanteButton = findViewById(R.id.buttonAjouterPlante);
        ajouterRecetteButton = findViewById(R.id.buttonAjouterRecette);
        spinnerPlante = findViewById(R.id.spinnerPlanteMain);
        spinnerFamille = findViewById(R.id.spinnerFamillleMain);

        final ArrayAdapter<Famille> adapterFamille = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, listeFamille);

        final ArrayAdapter<Plante> adapterPlante = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, listePlante);


        familleViewModel.getFamilleLD().observe(this, new Observer<List<Famille>>() {
            @Override
            public void onChanged(@Nullable final List<Famille> familles) {
                listeFamille.clear();
                for(Famille f : familles) {
                    listeFamille.add(f);
                }
                Log.d("famillelogs", "listfamille : " + listeFamille.toString());
                adapterFamille.notifyDataSetChanged();
            }
        });


        spinnerFamille.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                famIdPositon = 1 + pos;
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        adapterFamille.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFamille.setAdapter(adapterFamille);

        /* -------------------------------------------------------------------------------------*/

        planteViewModel.getPlanteLD().observe(this, new Observer<List<Plante>>() {
            @Override
            public void onChanged(@Nullable final List<Plante> plantes) {
                listePlante.clear();
                for(Plante plante : plantes) {
                    listePlante.add(plante);
                }
                adapterPlante.notifyDataSetChanged();
                Log.d("planteslogs", "listPlante : " + listePlante.toString());
            }
        });

        adapterPlante.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPlante.setAdapter(adapterPlante);


        spinnerPlante.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                planteIdPositon = 1 + pos;
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }


    public void validerBouttonFamille(View view){
        Intent intent = new Intent(this, FamillePlanteListeActivity.class);
        intent.putExtra("famille_plante", famIdPositon);
        startActivity(intent);
    }


    public void validerBouttonPlante(View view){
        Intent intent = new Intent(this, RecetteListePlanteActivity.class);
        intent.putExtra("recette_plante", planteIdPositon);
        startActivity(intent);
    }

    public void listeRecettes(View view){
        Intent intent = new Intent(this, RecetteListActivity.class);
        startActivity(intent);
    }

    public void listeFamilles(View view){
        Intent intent = new Intent(this, FamilleListeActivity.class);
        startActivity(intent);
    }

    public void listePlantes(View view){
        Intent intent = new Intent(this, PlantesListeActivity.class);
        startActivity(intent);
    }

    public void AjouterPlante(View view) {
        Intent intent = new Intent(this, PlanteMainActivity.class);
        startActivity(intent);
    }


    public void AjouterRecette(View view) {
        Intent intent = new Intent(this, RecetteMainActivity.class);
        startActivity(intent);
    }

    public void AjouterFamille(View view){
        Intent intent = new Intent(this, FamilleMainActivity.class);
        startActivity(intent);
    }

}