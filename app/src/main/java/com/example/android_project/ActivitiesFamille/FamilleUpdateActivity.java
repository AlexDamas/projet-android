package com.example.android_project.ActivitiesFamille;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android_project.ActivitiesRecette.RecetteListActivity;
import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.FamilleDAO;
import com.example.android_project.Model.Famille;
import com.example.android_project.Model.Recette;
import com.example.android_project.R;

public class FamilleUpdateActivity extends AppCompatActivity {

    private EditText nom, description;

    private FamilleDAO familleDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.famille_edit);

        nom = findViewById(R.id.editTextFamille);
        description = findViewById(R.id.editTextDescription);

        if(getIntent().hasExtra("updateFamille")){
            Famille famille = getIntent().getParcelableExtra("updateFamille");
            nom.setText(famille.getNom());
            description.setText(famille.getDescription());
            Log.d("meslogs", "OnCreate : " + famille.toString());
        }
        familleDAO = AppDatabase.getDatabaseInstance(this).getFamilleDAO();

    }

    public void saveFamille(View view){

        Famille famille = getIntent().getParcelableExtra("updateFamille");
        famille.setNom(nom.getText().toString());
        famille.setDescription(description.getText().toString());

        familleDAO.update(famille);
        Intent intent = new Intent(this, FamilleListeActivity.class);

        Toast.makeText(this, "Recette modifié !", Toast.LENGTH_SHORT).show();
        startActivity(intent);

    }

    public void showFamille(View view){
        finish();
    }
}