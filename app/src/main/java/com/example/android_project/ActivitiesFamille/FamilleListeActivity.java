package com.example.android_project.ActivitiesFamille;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.android_project.ActivitiesPlantes.PlanteDetailActivity;
import com.example.android_project.ActivitiesPlantes.PlanteUpdateActivity;
import com.example.android_project.ActivitiesPlantes.PlantesListeActivity;
import com.example.android_project.ActivitiesRecette.RecetteListActivity;
import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.FamilleDAO;

import com.example.android_project.MainActivity;
import com.example.android_project.R;
import com.example.android_project.Tools.Famille.FamilleAdapter;
import com.example.android_project.Tools.Famille.FamilleViewModel;
import com.example.android_project.Tools.ItemClickListener;
import com.example.android_project.Tools.Plante.PlanteAdapter;
import com.example.android_project.Tools.Plante.PlanteViewModel;

public class FamilleListeActivity extends AppCompatActivity implements ItemClickListener {

    RecyclerView recyclerView;
    FamilleDAO familleDAO;
    FamilleViewModel familleViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.famille_activity_liste);

        familleViewModel = new ViewModelProvider(this).get(FamilleViewModel.class);

        recyclerView = findViewById(R.id.familleRecyclerView);
        familleDAO = AppDatabase.getDatabaseInstance(this).getFamilleDAO();
        FamilleAdapter familleAdapter = new FamilleAdapter(familleDAO.getFamilles(),this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(familleAdapter);

    }


    @Override
    public void onShowClick(int position) {
        Log.d("meslogs", "famille test bouton cliqué show  " +position);
        Intent intent = new Intent(this, FamilleDetailActivity.class);
        intent.putExtra("famille_selected", familleDAO.getFamilles().get(position));
        startActivity(intent);
    }
    @Override
    public void onUpdateClick(int position) {
        Log.d("meslogs", "famille test bouton cliqué update  "+ position);
        Intent intent = new Intent(this, FamilleUpdateActivity.class);
        intent.putExtra("updateFamille", familleDAO.getFamilles().get(position));
        startActivity(intent);
    }


    @Override
    public void onDeleteClick(int position) {
        Log.d("meslogs", "famille test bouton cliqué delete  " + position );
        familleDAO = AppDatabase.getDatabaseInstance(this).getFamilleDAO();
        FamilleAdapter familleAdapter = new FamilleAdapter(familleDAO.getFamilles(),this);
        familleViewModel.delete(familleAdapter.getLaFamille(position));
        recreate();
    }

    //------------------------ BARRE MENU ------------------------------------------------------


    public void LienHome(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void listeRecettes(View view){
        Intent intent = new Intent(this, RecetteListActivity.class);
        startActivity(intent);
    }

    public void listeFamilles(View view){
        Intent intent = new Intent(this, FamilleListeActivity.class);
        startActivity(intent);
    }

    public void listePlantes(View view){
        Intent intent = new Intent(this, PlantesListeActivity.class);
        startActivity(intent);
    }

    //pour ajouter une famille via la barre de menu
    public void AjouterPlus(View view) {
        Intent intent = new Intent(this, FamilleMainActivity.class);
        startActivity(intent);
    }


    public void EditFamille(View view) {
    }
}

