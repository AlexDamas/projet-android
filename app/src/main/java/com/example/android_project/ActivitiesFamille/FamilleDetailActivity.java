package com.example.android_project.ActivitiesFamille;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android_project.BD.ConversionData;
import com.example.android_project.Model.Famille;
import com.example.android_project.Model.Plante;
import com.example.android_project.R;

public class FamilleDetailActivity extends AppCompatActivity {

    ImageView image;
    private Bitmap bitmapImage;
    TextView nom,description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.famille_activity_detail);

        image = findViewById(R.id.imageFamilleDetail);
        bitmapImage = null;
        nom = findViewById(R.id.familleNom);
        description = findViewById(R.id.familleDescription);

        if(getIntent().hasExtra("famille_selected")){
            Famille famille = getIntent().getParcelableExtra("famille_selected");
            nom.setText(famille.getNom());
            description.setText(famille.getDescription());
            image.setImageBitmap(ConversionData.convertirByteArrayVersImage(famille.getImage()));

            Log.d("meslogs", "test infos recup " + famille.toString());
        }
    }

    public void retourListeFamille(View v){ finish();}
}