package com.example.android_project.ActivitiesFamille;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android_project.ActivitiesPlantes.PlantesListeActivity;
import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.ConversionData;
import com.example.android_project.BD.FamilleDAO;
import com.example.android_project.MainActivity;
import com.example.android_project.Model.Famille;
import com.example.android_project.R;

public class FamilleMainActivity extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1 ;
    private ImageView imageView;
    private Bitmap bitmapImage;
    private EditText nom, description;

    FamilleDAO familleDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.famille_activity_main);

        imageView = findViewById(R.id.imageFamille);
        bitmapImage = null;
        nom = findViewById(R.id.editTextnomFamille);
        description = findViewById(R.id.editTextDescriptionFamille);

       familleDAO = AppDatabase.getDatabaseInstance(this).getFamilleDAO();

    }

//-------------------------------  SAUVEGARDER FAMILLE  -------------------------------------

    //sauvegarder les informations de la famille

    public void saveFamille(View view) {
        if(nom.getText().toString().isEmpty() || description.getText().toString().isEmpty()
                || bitmapImage == null){
            Toast.makeText(this, "Il manque des infos", Toast.LENGTH_SHORT).show();
        }else{
            Famille famille = new Famille();
            famille.setNom(nom.getText().toString());
            famille.setDescription(description.getText().toString());
            famille.setImage(ConversionData.convertirImageVersByteArray(bitmapImage));

            familleDAO.insert(famille);

            Toast.makeText(this, "Famille ajoutée !", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

        }
    }

    //---------------------------------- AFFICHER LES FamilleS ---------------------------------------

    //afficher la liste des familles créés

    public void showListeFamille(View view){
        Intent intent = new Intent(this, FamilleListeActivity.class);
        startActivity(intent);
    }


//-------------------------------  PRENDRE PHOTO  ----------------------------------------

    //Prendre une photo via l'appareil photo

    public void AjouterPhoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(intent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(intent,REQUEST_IMAGE_CAPTURE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            bitmapImage = (Bitmap) extras.get("data");
            imageView.setImageBitmap(bitmapImage);
        }
    }

}
