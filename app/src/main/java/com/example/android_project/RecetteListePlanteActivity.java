package com.example.android_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.RecetteDAO;

import com.example.android_project.Tools.Recette.RecetteViewModel;
import com.example.android_project.Tools.RecettePlante.RecettePlanteAdapter;

public class RecetteListePlanteActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecetteDAO recetteDAO;
    RecetteViewModel recetteViewModel;
    Integer planteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_activity_show_2);

        recetteViewModel = new ViewModelProvider(this).get(RecetteViewModel.class);

        Intent in = getIntent();
        Bundle b = in.getExtras();
        planteId = b.getInt("recette_plante");

        Log.d("logs", "Recette Liste plante " + planteId);

        recyclerView = findViewById(R.id.recetteRecyclerView);

        recetteDAO = AppDatabase.getDatabaseInstance(this).getRecetteDAO();

        RecettePlanteAdapter adapter = new RecettePlanteAdapter(recetteDAO.getRecetteByPlanteId(planteId));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }
}