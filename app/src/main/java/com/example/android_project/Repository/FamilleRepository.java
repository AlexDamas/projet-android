package com.example.android_project.Repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.android_project.BD.AppDatabase;
import com.example.android_project.Model.Famille;
import com.example.android_project.BD.FamilleDAO;
import com.example.android_project.Model.FamillePlantes;

import java.util.List;

public class FamilleRepository {

    private FamilleDAO familleDAO;
    private LiveData<List<Famille>> familleLD;
    private LiveData<String> nomFamille;

    public FamilleRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getDatabaseInstance(application);
        familleDAO = appDatabase.getFamilleDAO();
        familleLD = familleDAO.getFamilleLD();
    }

    public void insert(Famille famille) {
        new InsertAsyncTask(familleDAO).execute(famille);
    }

    LiveData<List<Famille>> getAllFamilles() {
        return familleLD;
    }

    public void update(Famille famille) {
        new UpdateAsyncTask(familleDAO).execute(famille);
    }

    public void delete(Famille famille) {
        new DeleteAsyncTask(familleDAO).execute(famille);
    }

    public void deleteAll() {
        new FamilleRepository.DeleteAllAsync(familleDAO).execute();
    }

    private static class DeleteAllAsync extends AsyncTask<Void, Void, Void> {
        private FamilleDAO familleDAO;

        public DeleteAllAsync(FamilleDAO t) {
            familleDAO = t;
        }

        @Override
        protected Void doInBackground(Void... args) {
            familleDAO.deleteAll();
            return null;
        }
    }


    private class OperationsAsyncTask extends AsyncTask<Famille, Void, Void> {

        FamilleDAO mAsyncTaskDao;

        OperationsAsyncTask(FamilleDAO dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Famille... familles) {
            return null;
        }
    }

    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(FamilleDAO mFamilleDao) {
            super(mFamilleDao);
        }

        @Override
        protected Void doInBackground(Famille... familles) {
            mAsyncTaskDao.insert(familles[0]);
            return null;
        }
    }

    private class UpdateAsyncTask extends OperationsAsyncTask {

        UpdateAsyncTask(FamilleDAO familleDAO) {
            super(familleDAO);
        }

        @Override
        protected Void doInBackground(Famille... familles) {
            mAsyncTaskDao.update(familles[0]);
            return null;
        }
    }

    private class DeleteAsyncTask extends OperationsAsyncTask {

        public DeleteAsyncTask(FamilleDAO familleDAO) {
            super(familleDAO);
        }

        @Override
        protected Void doInBackground(Famille... familles) {
            mAsyncTaskDao.delete(familles[0]);
            return null;
        }
    }

    public LiveData<List<Famille>> getFamilleLD(){return familleLD;}

    public List<Famille> getFamilles(){
        try{
            return new GetTasksAsync(familleDAO).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb repository getFamilles");
        }
        return null;
    }

    private static class GetTasksAsync extends AsyncTask<Void, Void, List<Famille>> {
        private FamilleDAO familleDAO;

        public GetTasksAsync (FamilleDAO t){
            familleDAO = t;
        }

        @Override
        protected List<Famille> doInBackground(Void ... args){
            return familleDAO.getFamilles();
        }
    }

}


