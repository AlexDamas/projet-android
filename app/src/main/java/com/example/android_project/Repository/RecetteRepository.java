package com.example.android_project.Repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.RecetteDAO;
import com.example.android_project.Model.Recette;

import java.util.List;

public class RecetteRepository {

    private RecetteDAO recetteDAO;
    private LiveData<List<Recette>> recettesLD;


    public RecetteRepository(Application application){
        AppDatabase appDatabase = AppDatabase.getDatabaseInstance(application);
        recetteDAO = appDatabase.getRecetteDAO();
        recettesLD = recetteDAO.getRecettesLD();
    }

    public Long insert(Recette recette){
        try {
            return new InsertAsyncTask(recetteDAO).execute(recette).get();
        }catch (Exception e){
            Log.d("MesLogs", "pb Insertion recette");
            return null;
        }
    }

    protected static class InsertAsyncTask extends AsyncTask<Recette, Void, Long>{
        private RecetteDAO recetteDAO;

        public InsertAsyncTask(RecetteDAO t){
            this.recetteDAO = t;
        }

        @Override
        protected Long doInBackground(Recette... recettes){
            return recetteDAO.insert(recettes[0]);
        }
    }

    public void update(final Recette recette){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                recetteDAO.update(recette);
            }
        });
    }

    public void delete(final Recette recette){
        new DeleteAsync(recetteDAO).execute(recette);
    }

    private static class DeleteAsync extends AsyncTask<Recette, Void, Void>{
        private RecetteDAO recetteDAO;

        public DeleteAsync (RecetteDAO t){
            recetteDAO = t;
        }

        @Override
        protected Void doInBackground(Recette... recette){
            recetteDAO.delete(recette[0]);
            return null;
        }
    }

    public LiveData<List<Recette>> getRecettesLD() {
        return recettesLD;
    }

    public List<Recette> getRecettes(){
        try{
            return new GetRecettesAsync(recetteDAO).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb liste de recette");
        }
        return null;
    }

    private static class GetRecettesAsync extends AsyncTask<Void, Void, List<Recette>>{
        private RecetteDAO recetteDAO;

        public GetRecettesAsync (RecetteDAO t) { recetteDAO = t;}

        @Override
        protected List<Recette> doInBackground(Void... args){return recetteDAO.getRecettes();}
    }

}
