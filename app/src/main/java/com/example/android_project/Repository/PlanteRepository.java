package com.example.android_project.Repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.PlanteDAO;
import com.example.android_project.Model.FamillePlantes;
import com.example.android_project.Model.Plante;

import java.util.List;

public class PlanteRepository{

    private PlanteDAO planteDao;
    private LiveData<List<Plante>> planteLD;
    private LiveData<Integer> nbPlantesLD;

    public PlanteRepository(Application application){
        AppDatabase appDatabase = AppDatabase.getDatabaseInstance(application);
        planteDao = appDatabase.getPlanteDAO();
        planteLD = planteDao.getPlantesLD();
        nbPlantesLD = planteDao.countLD();
    }

// --------------------------- ASYNTASKS METHODES CRUD -------------------------------------------------------

    //voir doc https://developer.android.com/reference/android/os/AsyncTask

    //------------------------- INSERT plante ----------------------

    public Long insert (Plante plante){
        try {
            return new InsertAsyncTask(planteDao).execute(plante).get();
        }catch (Exception e){
            Log.d("MesLogs", "pb Insertion recette");
            return null;
        }
    }

    protected static class InsertAsyncTask extends AsyncTask<Plante, Void, Long> {
        private PlanteDAO planteDao;

        public InsertAsyncTask (PlanteDAO p){
            planteDao = p;
        }

        @Override
        protected Long doInBackground(Plante... plantes){
            return planteDao.insertPlante(plantes[0]);
        }
    }

//-------------------------------- UPDATE -----------------------------------

    public void update (final Plante plante){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                planteDao.updatePlante(plante);
            }
        });
    }

//--------------------------------- DELETE ------------------------------------

    public void delete (final Plante plante){
        new DeleteAsync(planteDao).execute(plante);
    }

    private static class DeleteAsync extends AsyncTask<Plante, Void, Void>{
        private PlanteDAO planteDAO;

        public DeleteAsync (PlanteDAO t){
            planteDAO = t;
        }

        @Override
        protected Void doInBackground(Plante... plantes){
            planteDAO.deletePlante(plantes[0]);
            return null;
        }
    }

// -------------------------- DELETE ALL ---------------------------------------------

    public void deleteAll (){
        new DeleteAllAsync(planteDao).execute();
    }

    private static class DeleteAllAsync extends AsyncTask<Void, Void, Void>{
        private PlanteDAO planteDAO;

        public DeleteAllAsync (PlanteDAO t){
            planteDAO = t;
        }

        @Override
        protected Void doInBackground(Void ... args){
            planteDAO.deleteAll();
            return null;
        }
    }

//----------------------------------- COUNT ----------------------------------------------------

    public Integer count (){
        try {
            return new CountAsync(planteDao).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb count");
        }
        return null;
    }

    private static class CountAsync extends AsyncTask<Void, Void, Integer>{
        private PlanteDAO planteDAO;

        public CountAsync (PlanteDAO t){
            planteDAO = t;
        }

        @Override
        protected Integer doInBackground(Void ... args){
            return planteDAO.count();
        }
    }


    // -------------------------- FIN METHODE ASYNTASK -----------------------------------------------


    //LIVEDATA


    public LiveData<List<Plante>> getPlanteLD() {
        return planteLD;
    }

    public LiveData<Integer> getNbPlantesLD() {
        return nbPlantesLD;
    }






}


