package com.example.android_project.ActivitiesRecette;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.RecetteDAO;
import com.example.android_project.Model.Recette;
import com.example.android_project.R;
import com.example.android_project.Tools.Recette.RecetteViewModel;

public class RecetteDetailActivity extends AppCompatActivity {
    TextView nomRecette,preparation;

    RecetteDAO recetteDAO;

    RecetteViewModel recetteModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_activity_detail);

        nomRecette = findViewById(R.id.recetteNomRecette);
        preparation = findViewById(R.id.recettePreparation);

        // a faire pour passer par le repository
        recetteModel = new ViewModelProvider(this).get(RecetteViewModel.class);

        if(getIntent().hasExtra("showRecette")){
            Recette recette = getIntent().getParcelableExtra("showRecette");
            nomRecette.setText(recette.getNom());
            preparation.setText(recette.getPreparation());
            Log.d("meslogs", "OnCreate de show : " + recette.toString());
        }
        recetteDAO = AppDatabase.getDatabaseInstance(this).getRecetteDAO();

    }

    public void retourListeRecette(View v){
        finish();
    }

}
