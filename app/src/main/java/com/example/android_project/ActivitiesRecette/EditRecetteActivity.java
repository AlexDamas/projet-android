package com.example.android_project.ActivitiesRecette;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.RecetteDAO;
import com.example.android_project.Model.Recette;
import com.example.android_project.R;


public class EditRecetteActivity extends AppCompatActivity {

    private EditText nomRecette, preparation;

    private RecetteDAO recetteDAO;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_edit);

        nomRecette = findViewById(R.id.editTextRecetteNom);
        preparation = findViewById(R.id.editTextRecettePreparation);

        recetteDAO = AppDatabase.getDatabaseInstance(this).getRecetteDAO();

        if(getIntent().hasExtra("updateRecette")) {
            Recette recette = getIntent().getParcelableExtra("updateRecette");
            nomRecette.setText(recette.getNom());
            preparation.setText(recette.getPreparation());
        }

    }

    public void updateRecette(View view){

        Recette recette = getIntent().getParcelableExtra("updateRecette");
        recette.setNom(nomRecette.getText().toString());
        recette.setPreparation(preparation.getText().toString());
        recette.getId_recette();

        recetteDAO.update(recette);

        Intent intent = new Intent(this, RecetteListActivity.class);
        Log.d("meslogs", "updateRecettes : " + recette.toString());
        Toast.makeText(this, "Recette modifié !", Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }

    public void cancelUpdate(View view){
        finish();
    }

}
