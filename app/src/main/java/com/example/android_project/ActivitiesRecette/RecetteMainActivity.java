package com.example.android_project.ActivitiesRecette;


import android.content.Intent;

import android.os.Bundle;

import android.util.Log;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.android_project.BD.AppDatabase;

import com.example.android_project.BD.PlanteDAO;
import com.example.android_project.BD.RecetteDAO;

import com.example.android_project.MainActivity;
import com.example.android_project.Model.Famille;
import com.example.android_project.Model.Plante;
import com.example.android_project.Model.Recette;
import com.example.android_project.R;
import com.example.android_project.Tools.Recette.RecetteViewModel;

import java.util.List;


public class RecetteMainActivity extends AppCompatActivity {

    private EditText nom,preparation;

    RecetteDAO recetteDAO;

    RecetteViewModel recetteViewModel;

    List<Plante> planteList;

    private Spinner spinner;

    Integer planteIdPositon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_activity_new);

        nom = findViewById(R.id.editTextRecetteNom);
        preparation = findViewById(R.id.editTextRecettePreparation);
        spinner = findViewById(R.id.spinnerPlanteMain);

        recetteDAO = AppDatabase.getDatabaseInstance(this).getRecetteDAO();

        recetteViewModel = new ViewModelProvider(this).get(RecetteViewModel.class);

        planteList = AppDatabase.getDatabaseInstance(this).getPlanteDAO().getPlantes();

        ArrayAdapter<Plante> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, planteList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                planteIdPositon = 1 + pos;
                Log.d("planteslogs","position :" + pos + "planteIdPositon :" + planteIdPositon );
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    public void SaveRecette(View view) {
        if(nom.getText().toString().isEmpty() || preparation.getText().toString().isEmpty()){
            Toast.makeText(this, "Il manque des infos", Toast.LENGTH_SHORT).show();
        }else{

            Recette recette = new Recette();
            recette.setNom(nom.getText().toString());
            recette.setPreparation(preparation.getText().toString());
            recette.setPlanteId(planteIdPositon);

            recetteViewModel.insert(recette);

            Toast.makeText(this, "Recette ajoutée !", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    public void ShowRecette(View view) {
        Intent intent = new Intent(this, RecetteListActivity.class);
        startActivity(intent);
    }

}