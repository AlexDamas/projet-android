package com.example.android_project.ActivitiesRecette;


import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.android_project.ActivitiesFamille.FamilleListeActivity;
import com.example.android_project.ActivitiesPlantes.PlantesListeActivity;
import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.RecetteDAO;
import com.example.android_project.MainActivity;
import com.example.android_project.Model.Recette;
import com.example.android_project.R;
import com.example.android_project.Tools.Recette.RecetteAdapter;
import com.example.android_project.Tools.Recette.RecetteViewModel;



public class RecetteListActivity extends AppCompatActivity implements RecetteAdapter.OnRecetteUpdateListener, RecetteAdapter.OnRecetteDeleteListener, RecetteAdapter.OnRecetteShowListener {

    RecyclerView recyclerView;
    RecetteDAO recetteDAO;
    RecetteViewModel recetteViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_activity_show);

        recetteViewModel = new ViewModelProvider(this).get(RecetteViewModel.class);

        recyclerView = findViewById(R.id.recetteRecyclerView);
        recetteDAO = AppDatabase.getDatabaseInstance(this).getRecetteDAO();
        RecetteAdapter recetteAdapter = new RecetteAdapter(recetteDAO.getRecettes(),this, this,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recetteAdapter);


        }

        @Override
        public void onRecetteShowListener(int position) {
            //Log.d("meslogs", "appuie sur boutton show de liste" +" "+position);
            Intent intent = new Intent(this, RecetteDetailActivity.class);
            intent.putExtra("showRecette", recetteDAO.getRecettes().get(position));
            startActivity(intent);
        }

        @Override
        public void onUpdateClick(int position) {
            //Log.d("meslogs", "appuie sur boutton Click de liste" +" "+ position);
            Intent intent = new Intent(this, EditRecetteActivity.class);
            intent.putExtra("updateRecette", recetteDAO.getRecettes().get(position));
            startActivity(intent);
        }

        @Override
        public void onRecetteDeleteListener(Recette myRecette) {
            Log.d("meslogs", "appuie sur boutton delete de liste" +" "+ myRecette.toString());
            recetteDAO.delete(myRecette);
            Intent intent = new Intent(this, RecetteListActivity.class);
            startActivity(intent);
            Log.d("meslogs", "appuie sur boutton delete de liste" +" "+ myRecette.toString());
        }

    //------------------------------------ BARRE MENU -------------------------------------------------------


    public void LienHome(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void listeRecettes(View view){
        Intent intent = new Intent(this, RecetteListActivity.class);
        startActivity(intent);
    }

    public void listeFamilles(View view){
        Intent intent = new Intent(this, FamilleListeActivity.class);
        startActivity(intent);
    }

    public void listePlantes(View view){
        Intent intent = new Intent(this, PlantesListeActivity.class);
        startActivity(intent);
    }

    //pour ajouter une Recette via la barre de menu
    public void AjouterPlus(View view) {
        Intent intent = new Intent(this, RecetteMainActivity.class);
        startActivity(intent);
    }


}
