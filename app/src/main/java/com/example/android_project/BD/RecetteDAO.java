package com.example.android_project.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.android_project.Model.Recette;

import java.util.List;

@Dao
public interface RecetteDAO {

    @Insert
     Long insert(Recette recette);

    @Update
    void update(Recette recette);

    @Delete
    void delete(Recette recette);

    @Query("Select * from Recette")
    List<Recette> getRecettes();

    @Query("Select * from Recette")
    LiveData<List<Recette>> getRecettesLD();

    @Query("Select * from Recette where planteId=:id")
    List<Recette> getRecetteByPlanteId(int id);


}

