package com.example.android_project.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.android_project.Model.Plante;

import java.util.List;


@Dao
public interface PlanteDAO {

    @Insert
    Long insertPlante(Plante plante);

    @Update
    void updatePlante(Plante plante);

    @Delete
    void deletePlante(Plante plante);

    @Query("DELETE From TablePlante")
    void deleteAll();

    //récuperer toutes les plantes
    @Query("Select * from TablePlante")
    List<Plante> getPlantes();

    //LiveDATA
    @Query("Select * from TablePLante")
    LiveData<List<Plante>> getPlantesLD();

    @Query("Select count(*) from TablePlante")
    int count();

    @Query("SELECT count(*) from TablePlante")
    LiveData<Integer> countLD();

    @Query("Select * from TablePlante where familleId=:id")
    List<Plante> getPlanteByFamilleId(int id);



}
