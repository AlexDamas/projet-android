package com.example.android_project.BD;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.android_project.Model.Famille;
import com.example.android_project.Model.Plante;
import com.example.android_project.Model.Recette;

@Database(entities = {Plante.class, Recette.class, Famille.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE = null;

    public abstract PlanteDAO getPlanteDAO();
    public abstract RecetteDAO getRecetteDAO();
    public abstract FamilleDAO getFamilleDAO();

    public static synchronized AppDatabase getDatabaseInstance(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class,"plante_database").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

}