package com.example.android_project.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.android_project.Model.Famille;

import java.util.List;

@Dao
public interface FamilleDAO {

    @Insert
    void insert(Famille famille);

    @Update
    void update(Famille famille);

    @Delete
    void delete(Famille famille);

    @Query("DELETE From TableFamille")
    void deleteAll();

    @Query("Select * from TableFamille")
    List<Famille> getFamilles();

    @Query("Select * from TableFamille")
    LiveData<List<Famille>> getFamilleLD();

}


