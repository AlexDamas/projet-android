package com.example.android_project.BD;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class ConversionData {
    //conversion de l'image vers un tableau d'octect (array byte)
    public static byte[] convertirImageVersByteArray(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    //conversion d'un tableau d'octet vers le format image
    public static Bitmap convertirByteArrayVersImage(byte [] array){
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }


}
