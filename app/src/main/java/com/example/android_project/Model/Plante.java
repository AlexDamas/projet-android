package com.example.android_project.Model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import androidx.room.PrimaryKey;


import static androidx.room.ForeignKey.CASCADE;


@Entity(tableName = "TablePlante",foreignKeys =
        @ForeignKey(entity = Famille.class,
        parentColumns = "id_famille",
        childColumns = "familleId",
        onDelete = CASCADE))//

public class Plante implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    public int id_plante;
    @ColumnInfo(name = "nomPlante")
    public String nomCommun;
    @ColumnInfo(name = "nomScientifiquePlante")
    public String nomScientifique;
    @ColumnInfo(name = "descriptionPlante")
    public String description;
    public int familleId; //ajout de la clé etrangère

    public Plante () {
    }

    public int getFamilleId() {
        return familleId;
    }

    public void setFamilleId(int familleId) {
        this.familleId = familleId;
    }

    protected Plante(Parcel in) {
        id_plante = in.readInt();
        nomCommun = in.readString();
        nomScientifique = in.readString();
        description = in.readString();
        familleId = in.readInt();  //ajout
    }

    public static final Creator<Plante> CREATOR = new Creator<Plante>() {
        @Override
        public Plante createFromParcel(Parcel in) {
            return new Plante(in);
        }

        @Override
        public Plante[] newArray(int size) {
            return new Plante[size];
        }
    };

    public int getId_plante() {
        return id_plante;
    }

    public String getNomCommun() {
        return nomCommun;
    }

    public String getNomScientifique() {
        return nomScientifique;
    }

    public String getDescription() {
        return description;
    }

    public void setId_plante(int id_plante) {
        this.id_plante = id_plante;
    }

    public void setNomCommun(String nomCommun) {
        this.nomCommun = nomCommun;
    }

    public void setNomScientifique(String nomScientifique) { this.nomScientifique = nomScientifique; }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return nomCommun ;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id_plante);
        dest.writeString(nomCommun);
        dest.writeString(nomScientifique);
        dest.writeString(description);
        dest.writeInt(familleId); //ajout
    }

}
