package com.example.android_project.Model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "TableFamille")
public class Famille implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int id_famille;

    @ColumnInfo(name = "nomFamille")
    private String nom;
    @ColumnInfo(name = "description")
    private String description;
    //on va stocker l'image en BD en la convertissant en un tableau de byte (byte array)
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte [] image;


    public Famille(int id, String nom, String description){
        this.id_famille = id;
        this.nom = nom;
        this.description = description;
    }

    public Famille() {
    }

    protected Famille(Parcel in) {
        id_famille = in.readInt();
        nom = in.readString();
        description = in.readString();
        image = in.createByteArray();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id_famille);
        dest.writeString(nom);
        dest.writeString(description);
        dest.writeByteArray(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Famille> CREATOR = new Creator<Famille>() {
        @Override
        public Famille createFromParcel(Parcel in) {
            return new Famille(in);
        }

        @Override
        public Famille[] newArray(int size) {
            return new Famille[size];
        }
    };

    public int getId_famille() {
        return id_famille;
    }

    public void setId_famille(int id_famille) {
        this.id_famille = id_famille;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription(){return description;}

    public void setDescription(String description){this.description = description;}

    public byte[] getImage() { return image; }

    public void setImage(byte[] image) { this.image = image; }

    @Override
    public String toString() {
        return nom  ;
    }
}









