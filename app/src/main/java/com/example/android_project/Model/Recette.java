package com.example.android_project.Model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;


@Entity(foreignKeys = @ForeignKey(entity = Plante.class,
        parentColumns = "id_plante",
        childColumns = "planteId",
        onDelete = CASCADE))

public class Recette implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    public int id_recette;
    @ColumnInfo(name = "nomRecette")
    public String nom;
    @ColumnInfo(name = "preparation")
    public String preparation;
    public int planteId;

    public Recette(String nom, String preparation, int planteId){
        this.nom = nom;
        this.preparation = preparation;
        this.planteId = planteId;
    }

    public int getPlanteId() {
        return planteId;
    }

    public void setPlanteId(int planteId) {
        this.planteId = planteId;
    }

    public Recette(){}

    protected Recette(Parcel in) {
        id_recette = in.readInt();
        nom = in.readString();
        preparation = in.readString();
        planteId = in.readInt();
    }

    public static final Creator<Recette> CREATOR = new Creator<Recette>() {
        @Override
        public Recette createFromParcel(Parcel in) {
            return new Recette(in);
        }

        @Override
        public Recette[] newArray(int size) {
            return new Recette[size];
        }
    };

    public int getId_recette() {
        return id_recette;
    }

    public void setId_recette(int id_recette) {
        this.id_recette = id_recette;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }


    @Override
    public String toString() {
        return "Recette{" +
                "id=" + id_recette + '\'' +
                "nom='" + nom + '\'' +
                "preparation='" + preparation + '\'' +
                "planteId='" + planteId + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id_recette);
        dest.writeString(nom);
        dest.writeString(preparation);
        dest.writeInt(planteId);
    }
}
