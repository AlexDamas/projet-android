package com.example.android_project.Model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;


public class RecettePlante {

    @Embedded
    private Recette recette;

    @Relation(
            parentColumn = "id_recette",
            entityColumn = "recetteId"
    )

    public List<Recette> recetteList;

    public RecettePlante() {
    }

    public Recette getRecette() {
        return recette;
    }

    public void setRecette(Recette recette) {
        this.recette = recette;
    }

    public List<Recette> getRecetteList() { return recetteList; }


}

