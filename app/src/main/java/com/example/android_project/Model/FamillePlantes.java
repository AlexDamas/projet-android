package com.example.android_project.Model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;


public class FamillePlantes {

    @Embedded
    private Famille famille;

    @Relation(
            parentColumn = "id_famille",
            entityColumn = "familleId"
    )

    public List<Plante> planteList;

    public FamillePlantes() {
    }

    public Famille getFamille() {
        return famille;
    }

    public void setFamille(Famille famille) {
        this.famille = famille;
    }

    public List<Plante> getPlanteList() { return planteList; }

    public void setPlanteList(List<Plante> planteList) {
        this.planteList = planteList;
    }

}

