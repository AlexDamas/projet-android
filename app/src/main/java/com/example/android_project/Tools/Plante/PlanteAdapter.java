package com.example.android_project.Tools.Plante;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android_project.Model.Plante;
import com.example.android_project.R;

import com.example.android_project.Tools.ItemClickListener;

import java.util.ArrayList;
import java.util.List;


public class PlanteAdapter extends RecyclerView.Adapter<PlanteViewHolder> implements View.OnClickListener {

    List<Plante> LesPlantes;
    private ItemClickListener mItemClickListener;

    public PlanteAdapter (List<Plante> listePlantes, ItemClickListener itemClickListener){
        this.LesPlantes = listePlantes;
        this.mItemClickListener = itemClickListener;
    }


    //Create new views for RecyclerView (invoked by the layoutManager)
    @NonNull
    @Override
    public PlanteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int ViewType){
        // create a new view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.plante_item_layout,parent,false);
        PlanteViewHolder planteViewHolder = new PlanteViewHolder(view,mItemClickListener);
        return planteViewHolder;
    }

    //Replace the contents of a view (invoked by the layoutManager)
    @Override
    public void onBindViewHolder(@NonNull final PlanteViewHolder planteViewHolder, final int position) {
        //get element from the dataset at this position
        //replace the contents of the view with that element
        final Plante plante = LesPlantes.get(position);
        planteViewHolder.nom.setText(plante.getNomCommun());
        planteViewHolder.nomScientifique.setText(plante.getNomScientifique());
       // planteViewHolder.setListeners();

    }

    @Override
    public int getItemCount() {
        return LesPlantes.size();
    }

    @Override
    public void onClick(View v) {
    }

    //utile pour la suppression
    public Plante getLaPlante(int position){
            return  LesPlantes.get(position);

    }
}





