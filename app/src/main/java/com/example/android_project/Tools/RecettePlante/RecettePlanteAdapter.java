package com.example.android_project.Tools.RecettePlante;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_project.Model.Recette;
import com.example.android_project.R;

import java.util.List;
public class RecettePlanteAdapter extends RecyclerView.Adapter<RecettePlanteViewHolder> {

    private List<Recette> lesRecettes;

    public RecettePlanteAdapter(List<Recette> lesRecettes) {
        this.lesRecettes = lesRecettes;
    }

    @NonNull
    @Override
    public RecettePlanteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recette_item_layout_menu,parent,false);
        return new RecettePlanteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecettePlanteViewHolder recettePlanteViewHolder, final int position) {
        final Recette recette = lesRecettes.get(position);
        recettePlanteViewHolder.nom.setText(recette.getNom());
        recettePlanteViewHolder.preparation.setText(recette.getPreparation());
    }

    @Override
    public int getItemCount() {
        return lesRecettes.size();
    }
}