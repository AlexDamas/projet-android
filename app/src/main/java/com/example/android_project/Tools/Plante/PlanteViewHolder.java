package com.example.android_project.Tools.Plante;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_project.Model.Plante;
import com.example.android_project.Model.Recette;
import com.example.android_project.R;
import com.example.android_project.Tools.ItemClickListener;
import com.example.android_project.Tools.Plante.PlanteAdapter;

import java.util.List;


//Un ViewHolder permet de placer les objets dans les cellules de la RecyclerView
public class PlanteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView nom;
    TextView nomScientifique;
    ImageView iconeShow,iconeDelete,iconeEdit;
    Button showListeRecette;


    ItemClickListener itemClickListener;

    public PlanteViewHolder(@NonNull View itemView, ItemClickListener itemClickListener) {
        super(itemView);
        nom = itemView.findViewById(R.id.cardViewNomPlante);
        nomScientifique = itemView.findViewById(R.id.cardViewNomScientifiquePlante);
      //  cardViewPlante = itemView.findViewById(R.id.cardViewPlante); //pour clic sur l'item d'une liste mais changement d'avis
        iconeShow = itemView.findViewById(R.id.imageViewShow);
        iconeDelete = itemView.findViewById(R.id.imageViewDelete);
        iconeEdit = itemView.findViewById(R.id.imageViewEdit);
       // showListeRecette = itemView.findViewById(R.id.buttonListePlante);

        this.itemClickListener = itemClickListener;
        //itemView.setOnClickListener(this);
        iconeShow.setOnClickListener(this);
        iconeDelete.setOnClickListener(this);
        iconeEdit.setOnClickListener(this);
      //  showListeRecette.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imageViewShow:
                itemClickListener.onShowClick(getAdapterPosition());
                break;

            case R.id.imageViewEdit:
                itemClickListener.onUpdateClick(getAdapterPosition());
                break;

            case R.id.imageViewDelete:
                itemClickListener.onDeleteClick(getAdapterPosition());
                break;

            default:
                break;
        }

    }

}


