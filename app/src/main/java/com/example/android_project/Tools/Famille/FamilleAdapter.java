package com.example.android_project.Tools.Famille;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_project.BD.ConversionData;
import com.example.android_project.Model.Famille;
import com.example.android_project.Model.Plante;
import com.example.android_project.R;
import com.example.android_project.Tools.ItemClickListener;

import java.util.List;

public class FamilleAdapter extends RecyclerView.Adapter<FamilleViewHolder> implements View.OnClickListener {

    List<Famille> LesFamilles;
    private ItemClickListener mItemClickListener;

    public FamilleAdapter (List<Famille> listeFamille, ItemClickListener itemClickListener){
        this.LesFamilles = listeFamille;
        this.mItemClickListener = itemClickListener;
    }



    //Create new views for RecyclerView (invoked by the layoutManager)
    @NonNull
    @Override
    public FamilleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int ViewType){
        // create a new view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.famille_item_layout,parent,false);
        FamilleViewHolder familleViewHolder = new FamilleViewHolder(view,mItemClickListener);
        return familleViewHolder;


    }


    //Replace the contents of a view (invoked by the layoutManager)
    @Override
    public void onBindViewHolder(@NonNull final FamilleViewHolder familleViewHolder, final int position) {
        //get element from the dataset at this position
        //replace the contents of the view with that element
        final Famille famille = LesFamilles.get(position);
        familleViewHolder.nom.setText(famille.getNom());
        familleViewHolder.imageView.setImageBitmap(ConversionData.convertirByteArrayVersImage(famille.getImage()));

    }



    @Override
    public int getItemCount() {
        return LesFamilles.size();
    }

    @Override
    public void onClick(View v) {
    }

    //utile pour la suppression
    public Famille getLaFamille(int position){
        return  LesFamilles.get(position);

    }
}





