package com.example.android_project.Tools.FamillePlante;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_project.Model.Plante;
import com.example.android_project.R;

import java.util.List;

public class FamillePlanteAdapter extends RecyclerView.Adapter<FamillePlanteViewHolder>{
        private List<Plante> lesPlantes;

        public FamillePlanteAdapter(List<Plante> lesPlantes) {
            this.lesPlantes = lesPlantes;
        }


        @NonNull
        @Override
        public FamillePlanteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plante_item_layout_menu,parent,false);
            return new FamillePlanteViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull FamillePlanteViewHolder holder, int position) {
            final Plante plante = lesPlantes.get(position);
            //holder.famille.setText(plante.getFamilleId());
            holder.nom.setText(plante.getNomCommun());
            holder.nomScientifique.setText(plante.getNomScientifique());
            holder.description.setText(plante.getDescription());
        }

        @Override
        public int getItemCount() {
            return lesPlantes.size();
        }
    }

