package com.example.android_project.Tools.Famille;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.android_project.Model.Famille;
import com.example.android_project.Model.FamillePlantes;
import com.example.android_project.Model.Plante;
import com.example.android_project.Repository.FamilleRepository;
import com.example.android_project.Repository.PlanteRepository;

import java.util.List;


public class FamilleViewModel extends AndroidViewModel {
    private FamilleRepository familleRepository;

    public FamilleViewModel(@NonNull Application application) {
        super(application);
        familleRepository = new FamilleRepository(application);
    }

    public LiveData<List<Famille>> getFamilleLD(){
        return familleRepository.getFamilleLD();
    }

    public List<Famille> getFamilles() {
        return familleRepository.getFamilles();
    }

    public void insert(Famille famille){
         familleRepository.insert(famille);
    }

    public void update(Famille famille){
        familleRepository.update(famille);
    }

    public void delete(Famille famille){
        familleRepository.delete(famille);
    }

    public void deleteAll(){
        familleRepository.deleteAll();
    }


}



