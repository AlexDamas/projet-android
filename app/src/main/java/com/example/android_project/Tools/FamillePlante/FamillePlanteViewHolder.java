package com.example.android_project.Tools.FamillePlante;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_project.R;

public class FamillePlanteViewHolder extends RecyclerView.ViewHolder{

        TextView nom, nomScientifique, description;

        public FamillePlanteViewHolder(@NonNull View itemView) {
            super(itemView);
            nom = itemView.findViewById(R.id.cardViewNomPlante);
            nomScientifique = itemView.findViewById(R.id.cardViewNomScientifiquePlante);
            description = itemView.findViewById(R.id.cardViewDescriptionPlante);
        }
    }

