package com.example.android_project.Tools.RecettePlante;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_project.R;
import com.example.android_project.Tools.ItemClickListener;

public class RecettePlanteViewHolder  extends RecyclerView.ViewHolder {

    TextView nom;
    TextView preparation;


    public RecettePlanteViewHolder(@NonNull View itemView) {
        super(itemView);
        nom = itemView.findViewById(R.id.cardViewNomRecette);
        preparation = itemView.findViewById(R.id.cardViewNomPrepartion);
    }
}