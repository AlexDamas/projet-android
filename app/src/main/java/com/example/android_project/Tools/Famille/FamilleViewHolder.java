package com.example.android_project.Tools.Famille;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_project.Model.Famille;
import com.example.android_project.R;
import com.example.android_project.Tools.ItemClickListener;

import java.util.List;


//Un ViewHolder permet de placer les objets dans les cellules de la RecyclerView
public class FamilleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private List<Famille> familles;
    TextView nom;
    Button boutonShow,boutonDelete,boutonEdit;
    ImageView imageView;


    ItemClickListener itemClickListener;

    public FamilleViewHolder(@NonNull View itemView, ItemClickListener itemClickListener) {
        super(itemView);
        nom = itemView.findViewById(R.id.cardViewnomFamille);
        imageView = itemView.findViewById(R.id.cardViewImageFamille);
        boutonShow = itemView.findViewById(R.id.buttonShowFamille);
        boutonDelete = itemView.findViewById(R.id.buttonDeleteFamille);
        boutonEdit = itemView.findViewById(R.id.buttonUpdateFamille);

        this.itemClickListener = itemClickListener;
        //itemView.setOnClickListener(this);
        boutonDelete.setOnClickListener(this);
        boutonEdit.setOnClickListener(this);
        boutonShow.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.buttonShowFamille:
                itemClickListener.onShowClick(getAdapterPosition());
                break;

            case R.id.buttonUpdateFamille:
                itemClickListener.onUpdateClick(getAdapterPosition());
                break;

            case R.id.buttonDeleteFamille:
                itemClickListener.onDeleteClick(getAdapterPosition());
                break;

            default:
                break;
        }
    }

}


