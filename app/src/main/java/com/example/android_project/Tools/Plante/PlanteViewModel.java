package com.example.android_project.Tools.Plante;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.PlanteDAO;
import com.example.android_project.Model.FamillePlantes;
import com.example.android_project.Model.Plante;
import com.example.android_project.Repository.PlanteRepository;

import java.util.List;


public class PlanteViewModel extends AndroidViewModel {
    private PlanteRepository planteRepository;
    private AppDatabase database;
    PlanteDAO planteDao;
    LiveData<Integer> nbPlantesLD;

    public PlanteViewModel(@NonNull Application application) {
        super(application);
        planteRepository = new PlanteRepository(application);
        database = AppDatabase.getDatabaseInstance(application);
        planteDao = database.getPlanteDAO();
        nbPlantesLD = planteRepository.getNbPlantesLD();
    }

    public void update(Plante plante){
        planteRepository.update(plante);
    }

    public void delete(Plante plante){
        planteRepository.delete(plante);
    }

    public LiveData<List<Plante>> getPlanteLD(){ return planteRepository.getPlanteLD();
    }

    public LiveData<Integer> getNbPlantesLD() {
        return nbPlantesLD;
    }


}


