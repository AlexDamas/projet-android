package com.example.android_project.Tools;

public interface ItemClickListener {

    void onShowClick(int position);

    void onUpdateClick(int position);

    void onDeleteClick(int position);



}
