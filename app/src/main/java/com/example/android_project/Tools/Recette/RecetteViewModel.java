package com.example.android_project.Tools.Recette;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.android_project.BD.AppDatabase;
import com.example.android_project.BD.RecetteDAO;
import com.example.android_project.Model.Plante;
import com.example.android_project.Model.Recette;
import com.example.android_project.Repository.PlanteRepository;
import com.example.android_project.Repository.RecetteRepository;

import java.util.List;


public class RecetteViewModel extends AndroidViewModel {

    private RecetteRepository recetteRepository;
    private PlanteRepository planteRepository;
    LiveData<List<Recette>> recettesLD;
    private AppDatabase database;
    RecetteDAO recetteDAO;

    public RecetteViewModel(@NonNull Application application){
        super(application);
        recetteRepository = new RecetteRepository(application);
        planteRepository = new PlanteRepository(application);
        database = AppDatabase.getDatabaseInstance(application);
        recetteDAO = database.getRecetteDAO();
    }

    public Long insert(Recette recette){
        return recetteRepository.insert(recette);
    }

    public void insertPlante(Plante plante){ planteRepository.insert(plante); }

    public void delete(Recette recette) {recetteRepository.delete(recette);}

    public void update(Recette recette){recetteRepository.update(recette);}

    public LiveData<List<Recette>> getRecettesLD() { return recettesLD ;}



}
