package com.example.android_project.Tools.Recette;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_project.Model.Recette;
import com.example.android_project.R;

import java.util.List;

public class RecetteAdapter extends RecyclerView.Adapter<RecetteAdapter.RecetteViewHolder> {

    private List<Recette> lesRecettes;
    private OnRecetteUpdateListener mOnRecetteListener;
    private OnRecetteDeleteListener mOnRecetteDelete;
    private OnRecetteShowListener mOnRecetteShow;


    public RecetteAdapter(List<Recette> recettes, OnRecetteUpdateListener onRecetteListener, OnRecetteDeleteListener onRecetteDeleteListener, OnRecetteShowListener onRecetteShowListener) {
        this.lesRecettes = recettes;
        this.mOnRecetteListener = onRecetteListener;
        this.mOnRecetteDelete = onRecetteDeleteListener;
        this.mOnRecetteShow = onRecetteShowListener;
    }

    //Create new views for RecyclerView (invoked by the layoutManager)
    @NonNull
    @Override
    public RecetteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recette_item_layout,parent,false);
        return new RecetteViewHolder(view, mOnRecetteListener, mOnRecetteDelete, mOnRecetteShow );
    }

    //Replace the contents of a view (invoked by the layoutManager)
    @Override
    public void onBindViewHolder(@NonNull final RecetteViewHolder recetteViewHolder, int position){
        //get element from the dataset at this position
        //replace the contents of the view with that element
            final Recette recette = lesRecettes.get(position);
            recetteViewHolder.nom.setText(recette.getNom());
            recetteViewHolder.setListeners();
    }

    @Override
    public int getItemCount() {
        return lesRecettes.size();
    }


    public class RecetteViewHolder extends RecyclerView.ViewHolder {
        TextView nom;
        OnRecetteUpdateListener onRecettesListener;
        OnRecetteDeleteListener onRecetteDeleteListener;
        OnRecetteShowListener onRecetteShowListener;
        Button showButton,udpButton, deleteButton, addPlanteButton;

        public RecetteViewHolder(@NonNull final View itemView, OnRecetteUpdateListener onRecetteListener, OnRecetteDeleteListener onRecetteDeleteListener, OnRecetteShowListener onRecetteShowListener) {
            super(itemView);
            nom = itemView.findViewById(R.id.cardViewNomRecette);
            udpButton = itemView.findViewById(R.id.buttonUpdateRecette);
            deleteButton = itemView.findViewById(R.id.buttonDeleteRecette);
            showButton = itemView.findViewById(R.id.buttonShowRecette);

            this.onRecettesListener = onRecetteListener;
            this.onRecetteDeleteListener = onRecetteDeleteListener;
            this.onRecetteShowListener = onRecetteShowListener;

        }

        public void setListeners() {

            showButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d("meslogs", "showButton.setOnClickListener");
                    onRecetteShowListener.onRecetteShowListener(getAdapterPosition());
                    //Log.d("meslogs", "showButton.setOnClickListener 2");
                }
            });

            udpButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    onRecettesListener.onUpdateClick(getAdapterPosition());
                }
            });

            deleteButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if (onRecetteDeleteListener != null) {
                        Log.d("meslogs", "delete.setOnClickListener 1");
                        onRecetteDeleteListener.onRecetteDeleteListener(lesRecettes.get(getAdapterPosition()));
                        Log.d("meslogs", "delete.setOnClickListener 2");
                    }
                }
            });
        }

    }

    public interface OnRecetteUpdateListener{
        void onUpdateClick(int position);
    }

    public interface OnRecetteDeleteListener{
        void onRecetteDeleteListener(Recette myRecette);
    }

    public interface OnRecetteShowListener{
        void onRecetteShowListener(int position);
    }

}
